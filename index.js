require("dotenv").config();

const db = require("./helpers/db.js");
const bot = require("./helpers/bot.js");
const log = require("./helpers/log.js");
const admin_id = 295162096;

log("Bot started at " + new Date().toString());

bot.on('message', async (msg) => {
	let {from: user, chat} = msg;
	if(user == null) 
		return console.log("message.from equals null");;
	
	let udata = await db.users.add(user);
	
	if(udata == null) 
		return console.log("udata equals null");
	
	if(chat.type != "private" && db.admin_chats.indexOf(chat.id) == -1)
	       return await bot.leaveChat(chat.id);	
	
	await parseMessage(msg, udata);
});

async function parseMessage(msg, udata) {
	let {from:user, text, caption} = msg;
	let {uid} = udata;
	
	if(text == null && caption == null)
		return;
	if(text == null)
		text = "";
		
	if(text == "/start") {
		await bot.sendMessage(uid, await db.strings.get("start_text"), {
			parse_mode: "HTML"
		});
	} else if(uid != admin_id) {
		if(text != "/html_mode" && text != "/markdown_mode")
			await deformatMessage(msg, udata);
	} else {
		if(text.indexOf("/setstr ") != 0 && text.indexOf("/getstr ") != 0 && text != "/html_mode" && text != "/markdown_mode") {
			await deformatMessage(msg, udata);
		}
	}
	if(text == "/html_mode") {
		udata.mode = "HTML";
		await bot.sendMessage(uid, await db.strings.get("mode_updated_to_html"), {
			parse_mode: "HTML"
		});
		await db.users.set(udata);
	}
	
	if(text == "/markdown_mode") {
		udata.mode = "markdown";
		await bot.sendMessage(uid, await db.strings.get("mode_updated_to_markdown"), {
			parse_mode: "HTML"
		});
		await db.users.set(udata);
	}
	
	if(uid != admin_id)
		return;
		
	if(text.indexOf("/setstr ") == 0) {
		let args = text.substr(8);
		let m = args.match(/([a-z_0-9]+)\s.+/i);
		if(m != null) {
			let key = m[1];
			let text = args.substr(key.length).trim();
			await db.strings.set(key, text);
			await bot.sendMessage(uid, "OK\n\n/getstr " + key);
		} else {
			await bot.sendMessage(uid, "Invalid syntax");
		}
	}

	if(text.indexOf("/getstr ") == 0) {
		let args = text.substr(8);
		let str = await db.strings.get(args);
		await bot.sendMessage(uid,"/sestr " + args + " " + str);
	}
	
}

async function deformatMessage(msg, udata) {
	let {from:user} = msg;
	let entities = msg.entities || msg.caption_entities || [];
	let text = msg.text || msg.caption;
	let {uid, mode: type} = udata;
	
	
	let new_text = deformatText(text, entities, type);
	try {
		await bot.sendMessage(uid, new_text);
	} catch(e) {
		console.log(e);
		log(e);
		await bot.sendMessage(uid, await db.strings.get("something_went_wrong"));
	}
}


function deformatText(text, entities, type) {
	let tags;
		
	if(type == "HTML") {
		let j = 0;
		while((j = text.indexOf("<")) != -1) {
			for(let i = 0; i < entities.length; i++) {
				let entity = entities[i];
				if(entity.offset >= j) {
					entity.offset += 4;
				} else if(entity.offset <= j && (entity.offset+entity.length) >= j) {
					entity.length += 4;
				}
			}
			text = text.replace("<", "&lt;");
		}
		j = 0;
		while((j = text.indexOf(">")) != -1) {
			for(let i = 0; i < entities.length; i++) {
				let entity = entities[i];
				if(entity.offset >= j) {
					entity.offset += 4;
				} else if(entity.offset <= j && (entity.offset+entity.length) >= j) {
					entity.length += 4;
				}
			}
			text = text.replace(">", "&gt;");
		}
		
		tags = {
			bold: (text) => `<b>${text}</b>`, 
			strikethrough: (text) => `<s>${text}</s>`, 
			underline: (text) => `<u>${text}</u>`,
			text_mention: (text, entity) => `<a href="tg://user?id=${entity.user.id}">${text}</a>`,
			text_link: (text, entity) => `<a href="${entity.url}">${text}</a>`,
			pre: (text) => `<pre>${text}</pre>`,
			code: (text) => `<code>${text}</code>`,
			italic: (text) => `<i>${text}</i>`
		}
	} else {
		tags = {
			bold: (text) => `*${text}*`, 
			strikethrough: (text) => `~${text}~`, 
			underline: (text) => `__${text}__`,
			text_mention: (text, entity) => `[${text}](tg://user?id=${entity.user.id})`,
			text_link: (text, entity) => `[${text}](${entity.url})`,
			pre: (text) => `\`\`\`${text}\`\`\``,
			code: (text) => `\`${text}\``,
			italic: (text) => `_${text}_`
		}
	}
	
	for(let i = entities.length - 1; i >= 0; i--) {
		let entity = entities[i];
		for(let j = i - 1; j >= 0; j--) {
			if(entities[j].offset >= entity.offset && entities[j].offset + entities[j].length >= entity.offset + entity.length) {
				if(entities[j].childs === undefined)
					entities[j].childs = [];
				entities[j].childs.push(entity);
				delete entities[i];
				break;
			}
		}
	}
	entities = entities.filter(v => v !== undefined)
	
	let formatEntity = (entity, text) => {
		let subText = text.substring(entity.offset, entity.offset + entity.length);
		if(entity.childs !== undefined) {
			entity.childs.forEach((el) => {
				console.log(`en=${JSON.stringify(entity)}\nc=${JSON.stringify(el)}`)
				el.offset = el.offset - entity.offset ;
				
				let childText = formatEntity(el, subText);
				console.log(`eof=${entity.offset} el=${entity.length}\ncof=${el.offset} cl=${el.length}`)
				console.log(`part1=${subText.substring(0, el.offset)}\nchild=${childText}\npart2=${subText.substring(el.offset+el.length)}`)
				
				subText = subText.substring(0, el.offset) + childText + subText.substring(el.offset+el.length);
			});
		} 
		let f = tags[entity.type];
		if(f === undefined) {
			console.log(`unknown entity type="${entity.type}"`);
			return subText;
		} else {
			return f(subText, entity);
		}
	}
	
	for(let i = entities.length - 1; i >= 0; i--) {
		let entity = entities[i];
		let subText = formatEntity(entity, text);

		text = text.substring(0, entity.offset) + subText + text.substring(entity.offset+entity.length);
		console.log(`text="${text}"`);
	} 
	
	return text;
}



String.prototype.startWith = function(s) {
	if(!Array.isArray(s))
		return this.indexOf(s) == 0;
	else 
		return s.filter(e => this.indexOf(s) == 0).length > 0;
}


String.prototype.replaceAll = function(s, t) {
	return this.split(s).join(t);
}

String.prototype.enscapeHTML = function() {
	return this.replaceAll("<", "&lt;").replaceAll(">", "&gt;")
}
