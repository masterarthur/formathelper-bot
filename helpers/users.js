const formatUdata = require("./formatUdata.js");
const userToString = require("./userToString.js");

const log = require("./log.js");

module.exports = function(db) {
	return {
		_db: db,
		
		add: async function(user) {
			try {
				let uid = user.id;
				let udata = await this.get(uid);
				formatUdata(user)
				if(udata != null) {
					let uinfo = udata.userinfo;
					
					if(uinfo.first_name != user.first_name ||
					   uinfo.last_name != user.last_name ||
					   uinfo.username != user.username ) {
			
						udata.userinfo = user;
						await this._db.update({ uid: uid }, udata);
						
					}
				} else {
					udata = {
						uid: uid,
						userinfo: user,
						started: false,
						mode: "HTML"
					};
					
					log(userToString(udata.userinfo) + "[#id" + uid + "][" + (await this._db.count({})) + "] added to db at " + new Date().toString())
					await this._db.insert(udata);
				}
				return udata;
			} catch(e) {
				console.log(e);
				return null;
			}
		},
		
		set: async function(udata) {
			try {
				let uid = udata.uid;
				await this._db.update({ uid: uid }, udata, { upsert : true});
				return true;
			} catch(e) {
				console.log(e);
				return false;
			}
		},
		
		get: async function(id) {
			try {
				var udata = await this._db.findOne({ uid: id });
				if(udata != null)
					return udata
				else 
					return null;
			} catch(e) {
				console.log(e);
				return null;
			}
		},
		
		forEach: async function(f) {
			if(f == null)
				return;
			var all_users = await this._db.find({});
			for(let i = 0; i < all_users.length; i++) {
				if(f.constructor.name == "Function")
					f(all_users[i]);
				else if(f.constructor.name == "AsyncFunction")
					await f(all_users[i]);
			}
		}, 
		
		count: async function() {
			return await this._db.count({});
		},
		
		all: async function() {
			return await this._db.find({});
		}
	}
}
